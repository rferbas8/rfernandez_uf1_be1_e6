#### rfernandez_UF1_Be1_E6
<div style="color:#3BF18B">

## **UF1. Estructures seqüencials, alternatives i iteratives.**
</div>



## **Exercici 1:** **Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.**<br><br>
<div style="color:#4FDEEF">
Les iteracions son mol útils per crear un bucle mentre es compleixi la condició que has indicat, un cop finalitzi pot saltar a la següent. 

Exemple:

<br>

```c
    Iniciacio
    MIENTRE (expresio booleana)  fer { 
        Cos. 
    }
    Final. 
```

</div>
<br><br>


**Exercici 2: Disseny d’un algorisme que ens demani l’edat 100 vegades.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 10

int main()
{
    //variable
    int edat, contador;

    //inicialitzacio
    contador=1;

    while(contador<=MAX){
        //tractar
     printf("Introdueix la edat: ");
        scanf("%i",&edat);
     //obtenir seguent
     contador++;

    }
    return 0;
}
```
<br>

![Cap2](/imatges/cap2.png)<br><br>


**Exercici 3: Disseny d’un algorisme que mostri els 100 primers números naturals.**<br><br>

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main()
{
    int edat, comptador;

    //inicialitzacio
    comptador=1;
    while(comptador<=MAX){
        //tractar
        printf("%i, ",comptador);
        //obtenir següent
         comptador++;

    }
  return 0;
}
```
<br>

![Cap3](/imatges/cap3.png)<br><br>


**Exercici 4: Disseny d’un algorisme que mostri els nombres parells de l’1 al 100.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main()
{


    int edat, comptador;

    //inicialitzacio
    comptador=2;
    while(comptador<=MAX){
        //tractar
         printf("%i, ",comptador);
         //obtenir següent
         comptador=comptador+2;

    }
  return 0;
}
```
<br>

![Cap4](/imatges/cap4.png)<br><br>


**Exercici 5: Disseny d’un algorisme que mostri els nombres senars de l’1 al 100.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main()
{


    int edat, comptador;

    //inicialitzacio

    comptador=1;
    while(comptador<=MAX){
        //tractar
         printf("%i, ",comptador);
         //obtenir següent
         comptador=comptador+2;
    }
  return 0;
}
```
<br>

![Cap5](/imatges/cap5.png)<br><br>


**Exercici 6: Disseny d’un algorisme que mostri els nombres primers que hi ha entre l’1 i el 100 inclosos.**<br><br>
**Fet per mi**
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100
#define PIN_ACTUAL_BBDD 1234

int main()
{
    int n,comptador, esPrimer;

    for(n=1;n<=MAX;n++){
        esPrimer=1;
        comptador=2;
        while(comptador<=n /2&&esPrimer){
            if(n%comptador==0)
            esPrimer=0;
            comptador++;
        }
    if(esPrimer) printf(" %i ",n);
    }


    return 0;
}

```
<br>

![Cap6](/imatges/cap6.png)<br><br>

**


**Exercici 7: Disseny d’un algorisme que compti de 5 en 5 fins a 100.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main()
{


    int edat, comptador;

    //inicialitzacio

    comptador=5;
    while(comptador<=MAX){
        //tractar
         printf("%i, ",comptador);
         //obtenir següent
         comptador=comptador+5;
    }
  return 0;
}
```
<br>

![Cap7](/imatges/cap7.png)<br><br>

**Fet a classe**
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100
#define MAX_ELEMENTS 100
#define PIN_ACTUAL_BBDD 1234

int main()
{
    int elements[MAX_ELEMENTS+1];
    int i,j;
    //inicialitza vector
    for(i=1;i<MAX_ELEMENTS;i++) elements[i]=i;
    //Primer recorregut fins al MAX_ELEMNTNS
    for(i=2;i<MAX_ELEMENTS/2;i++){
        //Segon recorregut per cada elemrtnt del primer recorregut
        for(j=i+i;j<MAX_ELEMENTS;j=j+i){
            elements[j]=0;
        }
    }
    //Mostra resultat ssi no és 0...
    for(i=1;i<MAX_ELEMENTS;i++) if(elements[i]!=0) printf("%i, ",elements[i]);

    return 0;
}

```

**Exercici 8: Disseny d’un algorisme que vagi del número 100 al número 1 de forma descendent.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MIN 1

int main()
{


    int edat, comptador;

    //inicialitzacio

    comptador=100;
    while(comptador>=MIN){
        //tractar
        printf("%i, ",comptador);
        //obtenir següent
        comptador=comptador-1;
    }
  return 0;
}
```
<br>

![Cap8](/imatges/cap8.png)<br><br>


**Exercici 9: Disseny d’un algorisme que mostri “n” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, an = an-1 + an-2.**<br><br> 
```c
#include <stdio.h>
#include <stdlib.h>
#define MIN 1

int main()
{
	int nTermes, comptador;
	int n, n1, n2;
	//inicialitzacio
	comptador=3;


	printf("Quants termes de la successió de FIBONACCi vols que calculi? ");
		scanf("%i, ",&nTermes);
	if(nTermes>=1) printf("1, ");
	if(nTermes>=2) printf("1, ");
	if(nTermes>2){


	n1=1;
	n2=1;
		while(comptador<=nTermes){
		//tractar calcular un terme de la successop de FIBONACCI
			n=n1+n2;
			printf("%i, ",n);

			n2=n1;
			n1=n;


			//obtenir següent
			comptador++;
	
		}
	}
        return 0;
}
```
<br>

![Cap9](/imatges/cap9.png)<br><br>


**Exercici 10: A partir de la següent especificació:**
**Dissenyeu un algorisme que incrementi el temps en un segon.**

```c
{ anys=ANYS ^ dies=DIES ^
^hores=HORES ^ minuts= MINUTS ^ segons=SEGONS ^ anys>0 ^
^0<=dies<365 ^ 0<=hores<24 ^0<=minuts<60 ^ 0<=segons<60 }.
```
<br>

```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
    int anys, dies, hores, minuts, segons;
    anys=0;
    dies=0;
    hores=0;
    minuts=0;
    segons=0;

    while(1){
            segons++;
        if(segons==60){
            segons=0;
            minuts++;
        }
        if(minuts==60){
            minuts=0;
            hores++;
        }
        if(hores==24){
            hores=0;
            dies++;
        }
        if(dies==365){
            dies=0;
            anys++;
        }
        system("cls");
        printf("%.4i, %.3i, %.2i, %.2i, %.2i",anys,dies,hores,minuts,segons);
        sleep(1000);
    }


   return 0;
}
```
<br>

![Cap10](/imatges/cap10.png)<br><br>

<br><br>



**Exercici 11: Disseny d’un algorisme que calculi les solucions d’una equació de 2n grau.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int entrada[3];
    int a, b,c;
    float resultat;


      printf("Introdueix el valor que tindra C0:");
            scanf("%i",&entrada[0]);
      printf("Introdueix el valor que tindra r:");
            scanf("%i",&entrada[1]);
      printf("Introdueix el valor que tindra t:");
            scanf("%i",&entrada[2]);

    if(a==entrada[0]);
    if(b==entrada[1]);
    if(c==entrada[2]);

    printf("La teva combinacó de numeros es: %i, %i, %i, \n",entrada[0],entrada[1],entrada[2]);

        if(resultat = -entrada[1] + sqrt(pow(b,2)-4*a*c)/2*a||-entrada[1] - sqrt(pow(b,2)-4*a*c)/2*a);

    printf("El resultat de el Capital Final es:%f ",resultat);


    return 0;
}
```
<br>

![Cap11](/imatges/cap11.png)<br><br> 


**Exercici 12: Donat un text calcular la seva longitud. Recorda que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir  ’ara “text” i/o “paraula” ho llegirem com cadena de caràcters o string.**<br><br>
``` c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
   //EXERCUCU11
    //declarar
    int contador;
    char text[100];

    //inicialitzar
    contador=0;
    printf("Introdueix un text: ");
   	    scanf("%100[^\n]",text);

    while(text[contador]!='\0'){
        contador++;
    }
   	printf("El numero de caracters son: %i, ",contador);

     return 0;
}
```
<br>

![Cap12](/imatges/cap12.png)<br><br>


**Exercici 13: Donat un text comptar el nombre de vocals.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{

    int index,comptadorVocal;
    char text[100], vocals[5];
    vocals[0]='a';
    vocals[1]='e';
    vocals[2]='i';
    vocals[3]='o';
    vocals[4]='u';


    //inicialitzar
    index=0;
    comptadorVocal=0;
    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);

    while(text[index]!='\0'){
        if(text[index]==vocals[0]||
        text[index]==vocals[1]||
        text[index]==vocals[2]||
        text[index]==vocals[3]||
        text[index]==vocals[4]){
            comptadorVocal++;
        }
        index++;
    }
    printf("El numero de caracters son: %i, ",comptadorVocal);

   return 0;
}
```
<br>

![Cap13](/imatges/cap13.png)<br><br>


**Exercici 14: Donat un text comptar el nombre de consonants.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{

    int index,comptadorConsonants;
    char text[100], consonants[22];
    consonants[0]='b';
    consonants[1]='c';
    consonants[2]='d';
    consonants[3]='f';
    consonants[4]='g';
    consonants[5]='h';
    consonants[6]='j';
    consonants[7]='k';
    consonants[8]='l';
    consonants[9]='m';
    consonants[10]='ñ';
    consonants[11]='p';
    consonants[12]='q';
    consonants[13]='r';
    consonants[14]='s';
    consonants[15]='t';
    consonants[16]='v';
    consonants[17]='w';
    consonants[18]='x';
    consonants[19]='y';
    consonants[20]='z';

    //inicialitzar
    index=0;
    comptadorConsonants=0;
    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);

    while(text[index]!='\0'){
        if(text[index]==consonants[0]||
        text[index]==consonants[1]||
        text[index]==consonants[2]||
        text[index]==consonants[3]||
        text[index]==consonants[4]||
        text[index]==consonants[5]||
        text[index]==consonants[6]||
        text[index]==consonants[7]||
        text[index]==consonants[8]||
        text[index]==consonants[9]||
        text[index]==consonants[10]||
        text[index]==consonants[11]||
        text[index]==consonants[12]||
        text[index]==consonants[13]||
        text[index]==consonants[14]||
        text[index]==consonants[15]||
        text[index]==consonants[16]||
        text[index]==consonants[17]||
        text[index]==consonants[18]||
        text[index]==consonants[19]||
        text[index]==consonants[20]){
            comptadorConsonants++;
        }
        index++;
    }
    printf("El numero de consonants son: %i, ",comptadorConsonants);

   return 0;
}
```
<br>

![Cap14](/imatges/cap14.png)<br><br>


**Exercici 15: Donat un text capgirar-lo.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{

    char text[100], vocals[5], index ;



    //inicialitzar
    index=100;
    printf("Introdueix un text: ");
    scanf("%100[^\n]",text);

    while(text[index]>=0){
        printf("%c",text[index]);
        index=index-1;
    }
     return 0;
}
```
<br>

![Cap15](/imatges/cap15.png)<br><br>


**Exercici 16: Donat un text comptar el nombre de paraules que acaben en “ts”.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100


int main()
{

    //declarar
    int index, comptadorParaula;
    char text[MAX_TEXT+1];

    //inicialitzar
    printf("Introdueix un text: ");
    scanf("%100[^\n]",text);
    //esborrar espais del principi
    index=0;
    while(text[index]==' ') index++;

    comptadorParaula=0;
    while(text[index]!='\0'){
        while(text[index]==' ') index++;
        while(text[index]!=' ' && text[index]!='\0') index++;
        if(text[index-1]=='s' && text[index-2]=='t') comptadorParaula++;
    }
   	 printf("El numero de paraules son: %i, ",comptadorParaula);

     return 0;
}
```
<br>

![Cap16](/imatges/cap16.png)<br><br>


**Exercici 17: Donat un text comptar el nombre de paraules.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100


int main()
{

    //declarar
    int index, comptadorParaula;
    char text[MAX_TEXT+1];

    //inicialitzar
    printf("Introdueix un text: ");
    scanf("%100[^\n]",text);
    index=0;
    comptadorParaula=0;
    while(text[index]!='\0'){
        while(text[index]==' ') index++;
        while(text[index]!=' ' && text[index]!='\0') index++;
        comptadorParaula++;
    }
   	 printf("El numero de paraules son: %i, ",comptadorParaula);


     return 0;
}
```
<br>

![Cap17](/imatges/cap17.png)<br><br>


**Exercici 18: Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “as” dins del text.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100


int main()
{

    //declarar
    int index, comptadorAS;
    char text[MAX_TEXT+1];

    //inicialitzar
    printf("Introdueix un text: ");
    scanf("%100[^\n]",text);
    //esborrar espais del principi
    index=0;
    //while(text[index]==' ') index++;//saltar_blancs del principi


    comptadorAS=0;


    while(text[index]!='\0'){
        if(text[index]=='a' && text[index+1]=='s'){
            comptadorAS++;
        }
        index++;

    }
    printf("El numero de paraules que tenen as son: %i ",comptadorAS);


     return 0;
}
```
<br>

![Cap18](/imatges/cap18.png)<br><br>


**Exercici 19: Donat un text i una paraula (anomenada parbus). Dissenyeu unalgorisme que comprovi si la paraula és troba dins del text.**

**Nota:** *parbus és el nom de la variable que conté la paraula a cercar.*<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_PARAULA 100
#define MAX_BUSCA 100
#define bb while(getchar()!='\n')

int main()
{

    //declarar
    int index, indexi;
    char text[MAX_TEXT+1], paraula[MAX_PARAULA+1], busca[MAX_BUSCA+1];

    //inicialitzar
    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);bb;
    printf("Introdueix la paraula que vols trobar: ");
        scanf("%100[^\n]",paraula);bb;

    index=0;
    while(text[index]!='\0'){
        while(text[index]==' ') index++;//SALTAR BLANCS
        //obtenir paraula
        indexi=0;
        while(text[index]!='\0' && (text[index]!=' ')){
            busca[indexi]=text[index];
            index++;
            indexi++;
        }
        busca[indexi]='\0';
        //fi obtenir paraula
        //iguals paraula i indexi
        indexi=0;
        while(paraula[indexi]!='\0' && busca[indexi]!='\0'){
        if(paraula[indexi]!=busca[indexi])break;
        indexi++;
        }
        if(paraula[indexi]=='\0' && busca[indexi]=='\0'){
            printf("SON IGUALS");
            break;
        }

    }

     return 0;
}
```
<br>

![Cap19](/imatges/cap19.png)<br>

![Cap19](/imatges/cap19B.png)<br><br>




**Exercici 20: Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_PARAULA 100
#define MAX_BUSCA 100
#define bb while(getchar()!='\n')

int main()
{

    //declarar
    int index, indexi, comptador;
    char text[MAX_TEXT+1], paraula[MAX_PARAULA+1], busca[MAX_BUSCA+1];

    //inicialitzar
    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);bb;
    printf("Introdueix la paraula que vols trobar: ");
        scanf("%100[^\n]",paraula);bb;

    index=0;
    comptador=0;
    while(text[index]!='\0'){
        while(text[index]==' ') index++;//SALTAR BLANCS
        //obtenir paraula
        indexi=0;
        while(text[index]!='\0' && (text[index]!=' ')){
            busca[indexi]=text[index];
            index++;
            indexi++;

        }
        busca[indexi]='\0';
        //fi obtenir paraula
        //iguals paraula i indexi
        indexi=0;
        while(paraula[indexi]!='\0' && busca[indexi]!='\0'){
            if(paraula[indexi]!=busca[indexi])break;
            indexi++;

        }
        if(paraula[indexi]=='\0' && busca[indexi]=='\0'){
            comptador++;
        }
    }
    printf("El numero de paraules igual a la introduida son: %i",comptador);

     return 0;
}
```
<br>

![Cap20](/imatges/cap20.png)<br><br>


**Exercici 21: Donat un text i una paraula (parbus) i una paraula (parsub). Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).**

**Nota:** *parbus és el nom de la variable que conté la paraula a buscar iparsub el nom de la variable que conté la paraula que volem substituir.*

**Nota:** *afegiu captures de pantalla del resultat d’executar els diferents exercicis que demostrin la seva correctesa.*<br><br>
```c
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_TEXT2 100
#define MAX_PARBUS 100
#define MAX_PARSUB 100
#define MAX_PARTTMP 100
#define bb while(getchar()!='\n')

int main()
{
    int it1, it2, ib, itmp,is;
    char text[MAX_TEXT+1], text2[MAX_TEXT2+1];
    char parbus[MAX_PARBUS+1], parsub[MAX_PARSUB+1],parttmp[MAX_PARTTMP+1];

    printf("Introdueixu un text:");
        scanf("%100[^\n]",text);bb;
    printf("Introdueix la paraula que bols trobar:");
        scanf("%100[^\n]",parbus);bb;
    printf("Introdueix la paraula, la qual, substituira la anterior:");
        scanf("%100[^\n]",parsub);bb;

    it1=0;
    it2=0;
    while(text[it1]!='\0'){

    //obternir paraula
        while(text[it1]==' '){
            text2[it2]=text[it1];
            it1++;
            it2++;
        }

    itmp=0;
    while(text[it1]!=' '&& text[it1]!='\0'){
        parttmp[itmp]=text[it1];
        it1++;
        itmp++;
    }
    parttmp[itmp]='\0';

    ib=0;
    while(parttmp[ib]!='\0'||parbus[ib]!='\0'){
        if(parttmp[ib]!=parbus[ib])break;
        ib++;
    }
    if(parttmp[ib]=='\0'&&parbus[ib]=='\0');
    is=0;

    if(parttmp[ib]==parbus[ib]){
        while(parsub[is]!='\0'){
            text2[it2]=parsub[is];
            it2++;
            is++;
        }
    }

    else{
        while(parttmp[is]!='\0'){
            text2[it2]=parttmp[is];
            it2++;
            is++;10B7BD
    }

    text2[it2]='\0';

    }
    printf("El text que havia es: %s\n ",text);
    printf("\nEl text final es: %s ",text2);

     return 0;
}
```
<br>

![Cap21](/imatges/cap21.png)<br><br>

